package org.example;

import java.util.Objects;

public class Main {
    public static void main(String[] args) {

    }

    public static Boolean checkPalindrome(String word) {
        if (Objects.isNull(word) || word.trim().length() < 2) {
            return false;
        }
        String aux = "";
        for (int i=word.length()-1; i >= 0; i--) {
            aux += word.charAt(i);
        }
        return word.equals(aux);
    }

    public static void recursivePalindromeFinder(String word) {
        if (checkPalindrome(word)) {
            System.out.println(word);
            recursivePalindromeFinder(word.substring(1, word.length()-1));
        }
    }
}