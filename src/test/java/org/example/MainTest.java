package org.example;


import org.junit.Assert;
import org.junit.Test;

public class MainTest {

    @Test
    public void checkPalindromeValidScenario() {
        Boolean result = Main.checkPalindrome("oro");
        Assert.assertTrue(result);
        result = Main.checkPalindrome("aa");
        Assert.assertTrue(result);
        result = Main.checkPalindrome("  aa  ");
        Assert.assertTrue(result);
        result = Main.checkPalindrome("worow");
        Assert.assertTrue(result);
        result = Main.checkPalindrome("assa");
        Assert.assertTrue(result);
    }

    @Test
    public void checkPalindromeInvalidScenario() {
        Boolean result = Main.checkPalindrome("main");
        Assert.assertFalse(result);
        result = Main.checkPalindrome("");
        Assert.assertFalse(result);
        result = Main.checkPalindrome("a");
        Assert.assertFalse(result);
        result = Main.checkPalindrome(" ");
        Assert.assertFalse(result);
        result = Main.checkPalindrome("asd");
        Assert.assertFalse(result);
        result = Main.checkPalindrome("oros");
        Assert.assertFalse(result);
    }

    @Test
    public void checkPalindromeNull() {
        Boolean result = Main.checkPalindrome(null);
        Assert.assertFalse(result);
    }

    @Test
    public void checkRecursiveFinder(){
        Main.recursivePalindromeFinder("oro");
        Main.recursivePalindromeFinder("worow");
        Main.recursivePalindromeFinder("assa");
        Main.recursivePalindromeFinder("aa");
        Main.recursivePalindromeFinder("asd");
        Main.recursivePalindromeFinder("oros");

    }
}