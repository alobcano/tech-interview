package org.example;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

public class ClassTwoTest extends TestCase {

    @Test
    public void validScenario() {
        Integer[] list = {1,3,5,6,8,9,2,7,4,1,3,5,6,8,9,2,7,4};
        Integer[] expectedResult = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        ClassTwo.removeDuplicates(list);
        Assert.assertEquals(list,expectedResult);
    }

}